FROM debian:bullseye-slim as build
RUN apt-get update && apt-get install -y --no-install-recommends \
    openjdk-17-jdk \
    python3 \
    python3-pip \
    python3-venv \
    python3-dev \
    gcc \
    g++ \
    && rm -rf /var/lib/apt/lists/*

## Setup Python environment
RUN python3 -m venv /opt/venv && \
    . /opt/venv/bin/activate && \
    pip install wheel setuptools

## Install sysreview dependencies and build package
WORKDIR /usr/local/src/sysreview
ADD ./third-party/sysreview/requirements.txt .
RUN . /opt/venv/bin/activate && \
    pip install -r requirements.txt && \
    python -m spacy download en_core_web_sm
ADD ./third-party/sysreview ./
RUN . /opt/venv/bin/activate && pip install .

## Build jobert-comm package
WORKDIR /usr/local/src/jobert-comm
ADD setup.py ./
ADD src ./src/
RUN . /opt/venv/bin/activate && pip install .

## Build article-puller Jar
WORKDIR /usr/local/src/article-puller
ADD ./third-party/article-puller ./
RUN ./gradlew installDist && \
    mv build/install/article-puller/ /opt/


## Production stage
FROM debian:bullseye-slim
# gcc is needed for Numba and JIT compiler
RUN apt-get update && apt-get install -y --no-install-recommends \
    openjdk-17-jre \
    python3 \
    gcc \
    && rm -rf /var/lib/apt/lists/*
COPY --from=build /opt/ /opt/
ENV PATH="/opt/venv/bin:/opt/article-puller/bin:$PATH"
WORKDIR /sysreview
ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
