#!/bin/sh
if [ $# -ne 4 ]
then
    echo "Usage: $0 <ucnf> <corpus> <num trials> <max entries>"
    exit 1
fi

ARTICLES_FILE=/sysreview/data/articles.csv
DATA_DIR=`dirname "$ARTICLES_FILE"`

UCNF="$1"
CORPUS="$2"
LANGUAGES=en
NTRIALS="$3"
MAX_ENTRIES="$4"
DATABASE=arxiv
NTHREADS=$(nproc)

mkdir -p "$DATA_DIR"

if [ -z "$KAFKA_SERVER" ]
then
    KAFKA_SERVER="localhost:9092"
fi

>&2 echo "Retrieving articles..."
article-puller -o "$ARTICLES_FILE" \
    --database "$DATABASE" \
    --abstract \
    --max-entries "$MAX_ENTRIES" \
    --query "$UCNF"

>&2 echo "Starting clustering..."
sysreview $ARTICLES_FILE \
    --corpus "$CORPUS" \
    --language "$LANGUAGES" \
    --n_trials "$NTRIALS" \
    --n_threads "$NTHREADS" \
    --name data \
    --preprocess \
    --json \
    --kafka-server "$KAFKA_SERVER" \
    --ucnf "$UCNF"

>&2 echo "Sending data..."
jobert-comm "$DATA_DIR/df_hover.json" \
    --kafka-server "$KAFKA_SERVER" \
    --topic Pyrticles \
    --ucnf "$UCNF"
