import setuptools

setuptools.setup(
    name='jobert-comm',
    version='0.1',
    description='Communicator with Jobert using Kafka',
    url='#',
    author='Hugo Haldi',
    author_email='hugo.haldi@etu.unige.ch',
    packages=['jobert_comm'],
    package_dir={'jobert_comm': 'src/jobert-comm'},
    entry_points={
        'console_scripts': ['jobert-comm=jobert_comm.cli:main'],
    },
    install_requires=[
        'kafka-python',
        'pandas'
    ]
)
