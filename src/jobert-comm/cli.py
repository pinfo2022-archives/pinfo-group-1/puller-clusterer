import sys
import argparse

from kafka import KafkaProducer

import pandas as pd


def main() -> int:
    parser = argparse.ArgumentParser("jobert-comm")
    parser.add_argument(
        "file",
        help="file to send to Jobert ('-' for stdin)",
    )
    parser.add_argument(
        "--kafka-server",
        help="URL to the Kafka server",
        default="localhost:9092",
    )
    parser.add_argument(
        "--topic",
        help="Topic to be used on Kafka",
        default="Pyrticles"
    )
    parser.add_argument(
        "--ucnf",
        help="UCNF identifier of the result"
    )

    args = parser.parse_args()

    kafka_conf = {
        'bootstrap_servers': args.kafka_server,
        'max_request_size': 100_000_000,  # 100 MB limit for data transfer
    }

    try:
        producer = KafkaProducer(**kafka_conf)
        print("Connection to Kafka successfully established", file=sys.stderr)
    except:
        print(
            f"Kafka server not available: {args.kafka_server}", file=sys.stderr)
        return 1

    if args.file == "-":
        f = sys.stdin.buffer
    else:
        try:
            f = open(args.file, "rb")
        except:
            print(f"file {args.file} cannot be open", file=sys.stderr)
            return 1

    df = pd.read_json(f)
    df = df.reset_index()

    for _, article in df.iterrows():
        buff: bytes = article.to_json().encode()
        if len(buff) > kafka_conf['max_request_size']:
            print("article is too large", file=sys.stderr)
            return 1

        producer.send(args.topic, buff, args.ucnf.encode())

    print("Data sent", file=sys.stderr)

    return 0


if __name__ == "__main__":
    sys.exit(main())
